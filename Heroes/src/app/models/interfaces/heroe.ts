export interface Heroe {
  nombre: string;
  bio: String;
  img:String;
  debilidad: string;
  casa: string;
}
